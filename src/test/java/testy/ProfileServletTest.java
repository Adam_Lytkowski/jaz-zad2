package testy;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import web.ProfileServlet;

@RunWith(MockitoJUnitRunner.class)
public class ProfileServletTest extends Mockito {
	
	@Test
	public void shouldPrintProfileInformation() throws IOException, ServletException {
		// given
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		ProfileServlet test = new ProfileServlet();
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);

		// when
		test.doGet(request, response);

		// then
		verify(writer).println("<h1>Profile</h1>");
	}

}