package testy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import web.RegisterUser;

@RunWith(MockitoJUnitRunner.class)
public class RegisterUserTest extends Mockito {

	@Test
	public void shouldRedirectToLoginPageIfUsernameIsAvailableAndPasswordIsCorrect()
			throws IOException, ServletException {
		// given
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		RegisterUser test = new RegisterUser();

		// when
		when(request.getParameter("name")).thenReturn("Floki");
		when(request.getParameter("password")).thenReturn("1234");
		when(request.getParameter("confirmPassword")).thenReturn("1234");
		when(request.getParameter("email")).thenReturn("floki@gmail.com");
		test.doGet(request, response);

		// then
		verify(response).sendRedirect("log.jsp");
	}

	@Test
	public void shouldRefreshIfPasswordsAreNotMatching() throws IOException, ServletException {
		// given
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		RegisterUser test = new RegisterUser();

		// when
		when(request.getParameter("name")).thenReturn("ragnar");
		when(request.getParameter("password")).thenReturn("1234");
		when(request.getParameter("confirmPassword")).thenReturn("123467");
		when(request.getParameter("email")).thenReturn("ragnar@gmail.com");
		test.doGet(request, response);

		// then
		verify(response).sendRedirect("/registration.jsp");
	}
	
	@Test
	public void shouldRefreshIfUserIsAlreadyInDatabase() throws IOException, ServletException {
		// given
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		RegisterUser test = new RegisterUser();

		// when
		when(request.getParameter("name")).thenReturn("admin");
		when(request.getParameter("password")).thenReturn("1234");
		when(request.getParameter("confirmPassword")).thenReturn("1234");
		when(request.getParameter("email")).thenReturn("admin@o2.pl");
		test.doGet(request, response);

		// then
		verify(response).sendRedirect("/registration.jsp");
	}

}