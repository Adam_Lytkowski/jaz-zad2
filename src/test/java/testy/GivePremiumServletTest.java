package testy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import web.GivePremiumServlet;

@RunWith(MockitoJUnitRunner.class)
public class GivePremiumServletTest extends Mockito {
	
	@Test
	public void shouldRedirectToUserListServletIfUserIsInDatabase() throws IOException, ServletException {
		// given
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		GivePremiumServlet test  = new GivePremiumServlet();

		// when
		when(request.getParameter("name")).thenReturn("admin");
		when(request.getParameter("password")).thenReturn("1234");
		test.doGet(request, response);

		// then
		verify(response).sendRedirect("/UserListServlet");
	}

	

}