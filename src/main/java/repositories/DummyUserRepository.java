package repositories;

import java.util.ArrayList;
import java.util.List;

import domain.User;

public class DummyUserRepository implements UserRepository{

	private static List<User> db = new ArrayList<User>();
	
	
	static{
		User administrator = new User();
		administrator.setUsername("admin");
		administrator.setPassword("1234");
		administrator.setEmail("admin@o2.pl");
		administrator.setPrivileges("administrator");
		
		User ivar = new User();
		ivar.setUsername("ivar");
		ivar.setPassword("1234");
		ivar.setEmail("ivar@o2.pl");
		ivar.setPrivileges("regular");
		
		User ubbe = new User();
		ubbe.setUsername("ubbe");
		ubbe.setPassword("1234");
		ubbe.setEmail("ubbe@o2.pl");
		ubbe.setPrivileges("premium");
		
		User bjorn = new User();
		bjorn.setUsername("bjorn");
		bjorn.setPassword("1234");
		bjorn.setEmail("bjorn@o2.pl");
		bjorn.setPrivileges("regular");
		
		db.add(administrator);
		db.add(ivar);
		db.add(ubbe);
		db.add(bjorn);
	}
	
	
	
	
	@Override
	public boolean add(User user) {
		boolean isUserNew = db.stream().noneMatch(item -> {
			return item.getUsername().equals(user.getUsername()) && item.getEmail().equals(user.getEmail());
		});

		if (isUserNew) {
			db.add(user);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean login(User user) {
		for (User userDb : db) {
			if (userDb.getUsername().
					equals(user.getUsername()) && userDb.getPassword().equals(user.getPassword())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void givepremium(String username) {
		for(User dbuser: db){
			if(dbuser.getUsername().equals(username)){
				if(dbuser.getPrivileges()!="premium" && dbuser.getPrivileges()!="administrator"){
					dbuser.setPrivileges("premium");
				}
			}
		}	
	}

	@Override
	public List<User> getUsers() {
		return db;
	}

	@Override
	public String getPrivileges(User user){
		for(User dbuser:db){
			if(dbuser.getUsername().equals(((User) user).getUsername()))
				return dbuser.getPrivileges();
		}
		return null;
	}
	
	public String getEmail(User user) {
		for (User dbuser : db) {
			if (dbuser.getUsername().equals(user.getUsername())) {
				return dbuser.getEmail();
			}
		}
		return "There is no such user in database";
	}

	@Override
	public void removepremium(String username) {
		for(User dbuser: db){
			if(dbuser.getUsername().equals(username)){
				if(username!="admin"){
					dbuser.setPrivileges("regular");
				}
			}
		}	
		
	}

	

}