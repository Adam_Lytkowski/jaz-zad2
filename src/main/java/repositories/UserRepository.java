package repositories;

import java.util.List;

import domain.User;

public interface UserRepository {
public boolean add(User user);
public boolean login(User user);
public void givepremium(String username);
public void removepremium(String username);
public List<User> getUsers();
public String getPrivileges(User user);
public String getEmail(User user);
}
