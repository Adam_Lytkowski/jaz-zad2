package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.User;


public enum HyperSQLUserRepository implements UserRepository {

	INSTANCE;

	private static Connection connection;

	private static final String CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS users(username VARCHAR(40), password VARCHAR(40), email VARCHAR(40), privileges VARCHAR(40))";
	private static final String CLEAN_TABLE_QUERY = "DELETE FROM users";
	private static final String GET_ALL_USERS_QUERY = "SELECT * FROM users";
	private static final String ADD_USER_QUERY = "INSERT INTO users(username, password, email, role) VALUES(?,?,?,?)";
	private static final String LOGIN_VERIFICATION_QUERY = "SELECT * FROM users WHERE username=? AND password=?";
	private static final String GET_USER_DETAILS_QUERY = "SELECT * FROM users WHERE username=?";
	private static final String UPDATE_PRIVILEGES_QUERY = "UPDATE users SET privileges=? WHERE username=?";

	private static PreparedStatement addUserStatement;
	private static PreparedStatement loginVerificationStatement;
	private static PreparedStatement getUserDetailsStatement;
	private static PreparedStatement updatePrivilegesStatement;
	private static PreparedStatement getAllUsersStatement;

	private static Statement statement;

	private static final String PATH = "jdbc:hsqldb:hsql://localhost/workdb";

	static {
		try {
			connection = DriverManager.getConnection(PATH);
			statement = connection.createStatement();

			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			statement.executeUpdate(CREATE_TABLE_QUERY);
			statement.executeUpdate(CLEAN_TABLE_QUERY);

			addUserStatement = connection.prepareStatement(ADD_USER_QUERY);
			loginVerificationStatement = connection.prepareStatement(LOGIN_VERIFICATION_QUERY);
			getUserDetailsStatement = connection.prepareStatement(GET_USER_DETAILS_QUERY);
			updatePrivilegesStatement = connection.prepareStatement(UPDATE_PRIVILEGES_QUERY);
			getAllUsersStatement = connection.prepareStatement(GET_ALL_USERS_QUERY);
			
			User administrator = new User();
			administrator.setUsername("admin");
			administrator.setPassword("1234");
			administrator.setEmail("admin@o2.pl");
			administrator.setPrivileges("administrator");
			
			User ivar = new User();
			ivar.setUsername("ivar");
			ivar.setPassword("1234");
			ivar.setEmail("ivar@o2.pl");
			ivar.setPrivileges("regular");
			
			User ubbe = new User();
			ubbe.setUsername("ubbe");
			ubbe.setPassword("1234");
			ubbe.setEmail("ubbe@o2.pl");
			ubbe.setPrivileges("premium");
			
			User bjorn = new User();
			bjorn.setUsername("bjorn");
			bjorn.setPassword("1234");
			bjorn.setEmail("bjorn@o2.pl");
			bjorn.setPrivileges("regular");

			initUser(administrator);
			initUser(ivar);
			initUser(ubbe);
			initUser(bjorn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static HyperSQLUserRepository getInstance() {
		return HyperSQLUserRepository.INSTANCE;
	}

	@Override
	public List<User> getUsers() {
		List<User> users = new ArrayList<User>();

		try {
			ResultSet rs = getAllUsersStatement.executeQuery();

			while (rs.next()) {
				User user = new User();
				user.setUsername(rs.getString("username"));
				user.setEmail(rs.getString("email"));
				user.setPassword(rs.getString("password"));
				user.setPrivileges(rs.getString("privileges"));
				users.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;

	}

	@Override
	public boolean login(User user) {
		try {
			loginVerificationStatement.setString(1, user.getUsername());
			loginVerificationStatement.setString(2, user.getPassword());
			ResultSet rs = loginVerificationStatement.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String getPrivileges(User user) {
		try {
			getUserDetailsStatement.setString(1, user.getUsername());
			ResultSet rs = getUserDetailsStatement.executeQuery();
			rs.next();
			return rs.getString("privileges");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getEmail(User user) {
		try {
			getUserDetailsStatement.setString(1, user.getUsername());
			ResultSet rs = getUserDetailsStatement.executeQuery();
			rs.next();
			return rs.getString("email");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean add(User user) {
		int count = 0;
		try {
			addUserStatement.setString(1, user.getUsername());
			addUserStatement.setString(2, user.getPassword());
			addUserStatement.setString(3, user.getEmail());
			addUserStatement.setString(4, user.getPrivileges());

			count = addUserStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count > 0;
	}
	
	private static void initUser(User user) {
		int count = 0;
		try {
			addUserStatement.setString(1, user.getUsername());
			addUserStatement.setString(2, user.getPassword());
			addUserStatement.setString(3, user.getEmail());
			addUserStatement.setString(4, user.getPrivileges());

			count = addUserStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void givepremium(String username) {
		try {
			getUserDetailsStatement.setString(1, username);
			ResultSet rs = getUserDetailsStatement.executeQuery();
			
			String privileges = rs.getString("privileges");
			
			if(rs.getString("privileges").equals("regular")){
				updatePrivilegesStatement.setString(1, "premium");
				updatePrivilegesStatement.setString(2, username);
				updatePrivilegesStatement.executeUpdate();
			}
		}
			

		 catch (SQLException e) {
			e.printStackTrace();
		}
}

	@Override
	public void removepremium(String username) {
		try {
			getUserDetailsStatement.setString(1, username);
			ResultSet rs = getUserDetailsStatement.executeQuery();
			
			String privileges = rs.getString("privileges");
			
			if(rs.getString("privileges").equals("premium")){
				updatePrivilegesStatement.setString(1, "regular");
				updatePrivilegesStatement.setString(2, username);
				updatePrivilegesStatement.executeUpdate();
			}
		}
			

		 catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
	