package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repositories.DummyUserRepository;
import repositories.UserRepository;

@WebFilter({ "/LoginServlet", "/RegisterUser", "/log.jsp", "/registration.jsp" })
public class RegistrationAndLoginFilter implements Filter{
	
	public RegistrationAndLoginFilter() {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();
		
		if (session.getAttribute("user") != null) 
			((HttpServletResponse) response).sendRedirect("/ProfileServlet");
			
		if (chain != null) {
            chain.doFilter(request, response);
        }
	}
	
	
	public void init(FilterConfig fConfig) throws ServletException {
	}
}
