package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repositories.DummyUserRepository;
import repositories.UserRepository;

@WebFilter("/premium.jsp")
public class PremiumFilter implements Filter{
	
	public PremiumFilter() {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();
		
		if (session.getAttribute("user") == null) 
			((HttpServletResponse) response).sendRedirect("http://www.cusack.co.uk/imagecache/cb21f979-0445-435a-9a65-9f4f00bf1601_800x800.jpg");
		
		
		UserRepository repository = new DummyUserRepository();
		String privileges = repository.getPrivileges((User) session.getAttribute("user"));
		
		if(!(privileges.equals("administrator") || (privileges.equals("premium"))))
			((HttpServletResponse) response).sendRedirect("http://www.cusack.co.uk/imagecache/cb21f979-0445-435a-9a65-9f4f00bf1601_800x800.jpg");
		
			
		if (chain != null) {
            chain.doFilter(request, response);
        }
	}
	
	
	public void init(FilterConfig fConfig) throws ServletException {
	}
}
