package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.User;
import repositories.DummyUserRepository;
import repositories.HyperSQLUserRepository;
import repositories.UserRepository;

@WebServlet("/registration")
public class RegisterUser extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException {
		
		User newuser = retrieveUserFromRequest(request,response);
		UserRepository repository = new DummyUserRepository();
		//UserRepository repository = HyperSQLUserRepository.getInstance();
		 PrintWriter out=response.getWriter();
		
		if(repository.add(newuser)){
		response.sendRedirect("/log.jsp");
		}
			else{
				response.sendRedirect("/error.jsp");		
				}
		}	
	
	private User retrieveUserFromRequest (HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		if(request.getParameter("password").equals(request.getParameter("confirmpassword"))){
		User result = new User();
		result.setUsername(request.getParameter("username"));
		result.setPassword(request.getParameter("password"));
		result.setEmail(request.getParameter("email"));
		return result;
		}
			else{
				response.sendRedirect("/error.jsp");
				return null;
			}
	}
	}	

