package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import domain.User;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import repositories.DummyUserRepository;
import repositories.HyperSQLUserRepository;
import repositories.UserRepository;

@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public UserListServlet() {
		super();
	}
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
		PrintWriter out=response.getWriter();  
		UserRepository repository = new DummyUserRepository();
		//UserRepository repository = HyperSQLUserRepository.getInstance();
		
		List<User> allusers = repository.getUsers();
		out.println("<h1>All users list with privileges</h1>");
		out.println("<table>");
		out.println("<tr><th>Username</th><th>Email</th><th>Privileges</th></tr>");
		for (User user : allusers) {
			out.println("<tr><td>" + user.getUsername() + "</td>" + "<td>"
					+ user.getEmail() + "</td>" + "<td>" + user.getPrivileges() + "</td></tr>");
		}
		out.println("</table>");
		out.println("<a href='index.jsp'>Return to home page.</a>");
		
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
