package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repositories.DummyUserRepository;
import repositories.HyperSQLUserRepository;
import repositories.UserRepository;

@WebServlet("/ProfileServlet")
public class ProfileServlet extends HttpServlet{
	
	public ProfileServlet() {
		super();
	}
	
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)  
             throws ServletException, IOException {  
		
		PrintWriter out=response.getWriter();  
				 
		HttpSession session = request.getSession();
		UserRepository repository = new DummyUserRepository();
		//UserRepository repository = HyperSQLUserRepository.getInstance();
		
		User user = (User) session.getAttribute("user");
		
		out.println("<h1>Profile</h1>");
		out.println("<b>Username: </b>" + user.getUsername());
		out.println("<br/><b>Email: </b>" + repository.getEmail(user));
		out.println("<br/><b>Role: </b>" + repository.getPrivileges(user));
		out.println("<a href='index.jsp'>Return to home page.</a>");
	}
	
}
