package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repositories.DummyUserRepository;
import repositories.HyperSQLUserRepository;
import repositories.UserRepository;

@WebServlet("/log")
public class LoginServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	public LoginServlet(){
		super();
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException {
		
		  PrintWriter out=response.getWriter();  

		HttpSession session=request.getSession(); 
		String username = request.getParameter("username");
		String pwd = request.getParameter("password");
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(pwd);
		
		UserRepository repository = new DummyUserRepository();
		//UserRepository repository = HyperSQLUserRepository.getInstance();
		
		if(repository.login(user)){
			  
	        session.setAttribute("user",user);  
	        response.sendRedirect("/ProfileServlet");
			
		}
		else{
			response.sendRedirect("/log.jsp"); 
		}
		
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
