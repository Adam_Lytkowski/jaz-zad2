package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repositories.DummyUserRepository;
import repositories.HyperSQLUserRepository;
import repositories.UserRepository;

@WebServlet("/GivePremiumServlet")
public class GivePremiumServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	public GivePremiumServlet() {
		super();
	}
	
	
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();  
		String username = request.getParameter("username");
		
		UserRepository repository = new DummyUserRepository();
		//UserRepository repository = HyperSQLUserRepository.getInstance();
		
		if(request.getParameter("premium").equals("give")){
		repository.givepremium(username);
		}
		else{
			repository.removepremium(username);
		}
		response.sendRedirect("/UserListServlet");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
